
all:

dbinit: conf.d/model.sql
	@echo 'Initialize BookBee Database...'
	@cat $< | mysql -uroot

clean: dbinit

.PHONY: all
