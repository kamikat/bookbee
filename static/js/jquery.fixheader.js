
(function($){
    $.fn.extend({
        fixedHeaderSpy : function(fixclassname, paddingtop){
            // fix sub nav on scroll
            var $win = $(window)
            , $nav = this
            , navTop = $nav.length && $nav.offset().top - paddingtop
            , isFixed = 0;

            processScroll();

            $win.on('scroll', processScroll);

            $win.resize(function () {
                navTop = $nav.length && $nav.offset().top - paddingtop;
            });

            function processScroll() {
                if(! $nav.is(":visible")) return;
                var i, scrollTop = $win.scrollTop();
                navTop = $nav.length && $nav.offset().top - paddingtop;
                if(navTop + paddingtop == 0) return; 
                if (scrollTop >= navTop && !isFixed) {
                    isFixed = 1;
                    $nav.addClass(fixclassname);
                    $nav.trigger('fixHeader');
                } else if (scrollTop <= navTop && isFixed) {
                    isFixed = 0;
                    $nav.removeClass(fixclassname);
                    $nav.trigger('unfixHeader');
                }
            }
            
            this.updateClonedTable();

            return processScroll;
        },
        updateClonedTable: function () {
            var $nav = this;
            var $cloned_table = $nav.clone()
            .addClass("table-cloned-header")
            .removeAttr('id');
            var updateSize = function () {
                $cloned_table.width($nav.width());
                $cloned_table.height($nav.find("thead tr").height() + 2);
            };
            $nav
            .unbind('fixHeader')
            .unbind('unfixHeader');
            $(window).resize(updateSize);
            $nav.bind('fixHeader', function(){
                updateSize();
                $nav.parent().append($cloned_table);
            }).bind('unfixHeader', function(){
                $cloned_table.remove();
            });
        }
    });
})(jQuery);
