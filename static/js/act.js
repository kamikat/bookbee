
!(function ($, d) {

    var chain = [
    function () {
        // Schools List
        $.get(
            "/json/school/0.js",
            function (data) {
                d.school = data;
                chain.shift()();
            },
            "json"
        );
    },
    function () {
        // Books List
        $.get(
            "/json/book/0.js",
            function (data) {
                d.book = data;
                chain.shift()();
            },
            "json"
        );
    },
    function () {
        // Orders List
        $.get(
            "/json/order/0.js",
            function (data) {
                d.order = data;
                chain.shift()();
            },
            "json"
        );
    },
    function () {
        // Repository Information
        $.get(
            "/json/repository/0.js",
            function (data) {
                d.repo = data;
                chain.shift()();
            },
            "json"
        );
    },
    function () {
        // Calculate the count of each order
        _.each(d.book, function (item) {
            if(d.order[item.id] == undefined) return;
            var count = _(d.order[item.id]).values().reduce(
            function (a, b) {
                b = (b == "")?0:parseInt(b);
                return a + b;
            }, 0);
            count = (count == 0)?"":count;
            d.order[item.id].count = count;
        });
        chain.shift()();
    },
    function () {
        // Generate Home Page
        var c_order = _.size(d.order), 
            c_repo = _.size(d.repo);
        var ratio_repo_order = Math.round(c_repo * 100 / c_order);
        $("#order-count").text(c_order);
        $("#repository-count").text(c_repo);
        $("#progress-repo-order .pull-right").text(ratio_repo_order + "%");
        $("#progress-repo-order .bar").css({width: ratio_repo_order + "%"});

        var p_repo = 0, 
            p_order = 0;
        p_repo = _(d.repo).reduce(function (a, b) {
            return a + b.price * b.amount;
        }, 0);
        p_order = _(d.repo).keys().reduce(function (a, b) {
            var p = d.repo[b]
            p = p.price;
            if(p == undefined)
                return a;
            b = d.order[b];
            if(b == undefined)
                b = 0;
            else
                b = b.count;
            return a + b * p;
        }, 0);
        $("#repository-value").text(p_repo.toFixed(2));
        $("#order-value").text(p_order.toFixed(2));

        chain.shift()();
    },
    function () {
        // Generate Order Table
        $("#order-table thead").empty()
        .html(window.tpl('order-header', {
            schools: d.school
        }));
        var buf = [];
        _.each(d.book, function (item) {
            var order = d.order[item.id];
            if(order == undefined) order = {};
            buf.push(window.tpl('order-item', {
                book: item,
                schools: d.school,
                order: order,
            }));
        });
        $("#order-table tbody").empty()
        .html(buf.join(''));
        chain.shift()();
    },
    function () {
        // Generate Repository Table
        var buf = [];
        _.each(_.sortBy(d.book,
                        function(data) {
                            return data.id;
                        }
                       ), function (item) {
            if(item.id == '') return;
            var ri = d.repo[item.id];
            if(ri == undefined) {
                ri = {};
            }else{
                $("#order-" + item.id + " td:nth-child(2)").prepend($("<span />").addClass("label label-info").text("到货"));
            }
            buf.push(window.tpl('repo-item', {
                book: item,
                item: ri,
            }));
        });
        $("#repo-table tbody").empty()
        .html(buf.join(''));
        chain.shift()();
    },
    function () {
        // Generate Schedule Table
        $("#schedule-table thead").empty()
        .html(window.tpl('schedule-header', {
            schools: d.school
        }));
        var buf = [];
        _.each(_.sortBy(d.book,
                        function(data) {
                            return data.id;
                        }
                       ), function (item) {
            if(item.id == '') return;
            var ri = d.repo[item.id];
            var order = d.order[item.id];
            if(ri == undefined) return;
            if(order == undefined) return;
            buf.push(window.tpl('schedule-item', {
                book: item,
                schools: d.school,
                order: order,
                repo: ri,
            }));
        });
        $("#schedule-table tbody").empty()
        .html(buf.join(''));
        chain.shift()();
    },
    function () {
        // Generate Analyse Table
        var buf = [];
        var failed = 0, failval = 0, failp = 0;
        _.each(_.sortBy(d.book,
                        function(data) {
                            return data.id;
                        }
                       ), function (item) {
            if(item.id == '') return;
            var ri = d.repo[item.id];
            if(ri == undefined) return;
            var order = d.order[item.id];
            if(order == undefined) return;
            var diffval = d.order[item.id].count - ri.amount;
            if(diffval == 0) return;
            $("#order-" + item.id + " td:nth-child(2) span").after($("<span />").addClass("label label-important").text("差错"));
            failed++;
            var diffp = diffval * ri.price;
            failval += Math.abs(diffval);
            failp += Math.abs(diffp);
            var analyse = '无';
            // TODO Implement Analyse of clue
            buf.push(window.tpl('analyse-item', {
                book: item,
                order: order,
                repo: ri,
                diff: diffval,
                pricediff: diffp,
                analyse: analyse,
            }));
        });
        var analyse_result = '未发现数据不一致';
        if(failed > 0) {
            analyse_result = '共发现错误' + failed + '项，' +
                '绝对误差' + failval + '册，' + 
                '绝对价值' + failp.toFixed(2);
        }
        $("#analyse-result").text(analyse_result);
        $("#analyse-table tbody").empty()
        .html(buf.join(''));
        chain.shift()();
    },
    function () {
        /* Done */ 
        $("#repo-table").updateClonedTable();
        $("#order-table").updateClonedTable();
        $("#analyse-table").updateClonedTable();
        $("#schedule-table").updateClonedTable();
    }
    ];
    chain.shift()();
    
})(jQuery, {});
