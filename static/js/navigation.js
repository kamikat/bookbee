
!function ($) {

    $(function () {
        
        // By DEFAULT, Bind Click Event to the toggle of content
        var $navlinks = $(".navbar-fixed-top ul.nav li")
        .each(function (i, li) {
            var n = $("a", li);
            if(n == undefined) return;
            var target = (n[0].id.substring(n[0].id.indexOf("-") + 1));
            if(target != undefined && target != "") {
                // console.debug(target);
                n.click(function () {
                    $navlinks.removeClass("active");
                    $(li).addClass("active");
                    $frame = $("#" + target + "-frame");
                    $("#content").children().hide();
                    if($frame.length != 0) {
                        $frame.show();
                    }
                    // Dropdown
                    if($(li).parent().parent().hasClass("dropdown")) {
                        $(li).parent().parent().addClass("active");
                    }
                    // Pass the click event to body
                    $("body").click();
                    return false;
                });
            }
        });

        $("#navi-logout").unbind("click").click(function () {
            // Perform Logout Operations
            return false;
        });

        // This work should be taken by session manage module
        $("#navi-home").click();

    })

}(window.jQuery);
