!(function () {
// UI Process

var registerTable = function (func) {
    var processScroll = _.wrap(
        $("#" + func + "-table").fixedHeaderSpy("header-fixed", 47)
    , _.defer);
    $("#navi-" + func).click(function () {
        processScroll();
        return false;
    });
};

registerTable('order');
registerTable('repo');
registerTable('schedule');
registerTable('analyse');
})();
