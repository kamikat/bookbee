# BookBee

**BookBee** is a SPA(Single Page Application) based on [Twitter Bootstrap](http://twitter.github.com/bootstrap/).
This system aimed at the implementation of a system for manage and monitor the
distribution of textbooks/teaching material.
Mainly designed for easy operation and clear look of the workflow of textbook
distribution jobs.

## Functions

* **Book Information Import/Export** - Import data of subscribed books from
  csv file. Or export the book data. Edit of single book item come into
  consideration soon.
* **School Information Edit** - Allow user to edit units subscribe the order
  for textbooks.
* **Order Management** - Input order information via keyboard or import data
  through uploading a csv file with specific model.
* **Repository Management** - Inspect the repository information, manage the
  incoming textbooks, outgoing textbooks. Also support import action.
* **Order Check** - Check the consistency between orders and repository data.
  Give some human-readable advices on possible situations.
* **Print Report** - Generate a html based report.
* **Ckeck List** - This function is for work-time read and synchronzie of
  distrubution progress. Display a table with book items and their count to
  each unit, mark the item as empty/board/distributed states
* **Working Conclusion** - Conclude the Cleck List results

## Support Report Types

* **Classic One** - No clear definition > <.

## System Roles

There are three roles available in the system:

* **Adnimistrator** - Role has permissions to access all function
* **Operator** - Role has permission to edit Repository data and Order
* **Client** - Can read limited number of data. Client Account is for clients
  to issue order and query order state
* **Guest** - People with no session

Upper roles can has all access to permission lower roles owned
