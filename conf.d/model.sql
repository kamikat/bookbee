SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `bookbee` ;
CREATE SCHEMA IF NOT EXISTS `bookbee` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `bookbee` ;

-- -----------------------------------------------------
-- Table `bookbee`.`book`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bookbee`.`book` (
  `idbook` INT NOT NULL ,
  `id` VARCHAR(45) NULL ,
  `name` VARCHAR(45) NULL ,
  `press` VARCHAR(45) NULL ,
  PRIMARY KEY (`idbook`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `bookbee`.`tag`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bookbee`.`tag` (
  `idtag` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  PRIMARY KEY (`idtag`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `bookbee`.`school`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bookbee`.`school` (
  `idschool` INT NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  `contactor` VARCHAR(45) NULL ,
  `tel` VARCHAR(45) NULL ,
  PRIMARY KEY (`idschool`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `bookbee`.`order`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bookbee`.`order` (
  `book_idbook` INT NOT NULL ,
  `school_idschool` INT NOT NULL ,
  `amount` INT NULL ,
  `completed` INT NULL ,
  PRIMARY KEY (`book_idbook`, `school_idschool`) ,
  INDEX `fk_order_book1_idx` (`book_idbook` ASC) ,
  INDEX `fk_order_school1_idx` (`school_idschool` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookbee`.`burst`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bookbee`.`burst` (
  `idburst` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  `time` DATETIME NULL ,
  PRIMARY KEY (`idburst`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `bookbee`.`repo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bookbee`.`repo` (
  `book_idbook` INT NOT NULL ,
  `price` DECIMAL(10,0) NULL ,
  `amount` INT NULL ,
  `package2bundle` INT NULL ,
  `bundle2piece` INT NULL ,
  `acked` INT NULL COMMENT 'The Amount that acknowledged to be exist' ,
  `burst_idburst` INT NOT NULL ,
  PRIMARY KEY (`book_idbook`) ,
  INDEX `fk_repo_book2_idx` (`book_idbook` ASC) ,
  INDEX `fk_repo_burst1_idx` (`burst_idburst` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookbee`.`book_has_tag`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bookbee`.`book_has_tag` (
  `book_idbook` INT NOT NULL ,
  `tag_idtag` INT NOT NULL ,
  PRIMARY KEY (`book_idbook`, `tag_idtag`) ,
  INDEX `fk_book_has_tag_tag1_idx` (`tag_idtag` ASC) ,
  INDEX `fk_book_has_tag_book1_idx` (`book_idbook` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookbee`.`plan`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bookbee`.`plan` (
  `idplan` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idplan`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

USE `bookbee` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
