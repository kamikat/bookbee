
// Utility Function
// ================

var should = require('should');
var _ = require('underscore');

var deferred = require('deferred');
var promisify = deferred.promisify;

// $P - Safe invocation wrapper function {{{

var $P = (function () {

    var trigger = require('trigger.js')

    var invoker = function (fn) {
        return invoker.m.wrap('initialized', fn);
    };
    invoker.m = trigger('unknown');
    invoker.initialize = function () {
        invoker.m('initialized');
    };

    return invoker;

})();

// }}}

var mysqlconf = require('../conf.d/mysqlconn.json');
var mysql = require('mysql');

var SQL = require('./sql');

// exec - SQL execution utility function (Promise Styled) {{{

var pool = mysql.createPool({

    createConnection: function () {

        var connection =  mysql.createConnection(mysqlconf);
        connection.config.queryFormat = SQL.CommandResolver;

        return connection;

    }

});

var _exec = function () {

    var args = arguments;
    var callback = args[args.length-1];

    pool.getConnection(function (err, connection) {

        args[args.length-1] = function (err, result) {
            callback(err, result);
            connection.end();
        };

        var sql = connection.query.apply(connection, args);

        console.log(sql.sql);

    });

};

var exec = promisify(_exec);

// }}}

// Initialization
// ==============

// {{{

var plans = [];

exec(SQL.PLAN_GET)
.end(function (result) {

    // Read plans to initialize
    plans = result;

    $P.initialize();

});

/// }}}

// Export Functions
// ================

var fn = {

    // Export the connection pool API
    pool: pool,

    // Export the query API
    exec: _exec,

    // Export the Safety Call Wrapper
    "$P": $P,

};

// Database Operation Definition
// =============================

// Plan Operation {{{

fn.plan = function (cb) {

    $P(function () {

        cb(null, plans);

    })();

    return plans;

};

_(fn.plan).extend({

    add: $P(function (name, cb) {

        // First, add to plan table
        exec(SQL.PLAN_ADD, {
            name: name
        })
        // Then, update the plans array
        (function (result) {

            var record = {
                idplan: result.insertId,
                name: name
            };

            plans.push(record);

            var V = {plan: record.idplan};
            
            // WTF! Can't get this into the defer chain!
            deferred(
                exec(SQL.PLAN_TABLE_CREATE('book')          , V),
                exec(SQL.PLAN_TABLE_CREATE('tag')           , V),
                exec(SQL.PLAN_TABLE_CREATE('book_has_tag')  , V),
                exec(SQL.PLAN_TABLE_CREATE('school')        , V),
                exec(SQL.PLAN_TABLE_CREATE('order')         , V),
                exec(SQL.PLAN_TABLE_CREATE('repo')          , V),
                exec(SQL.PLAN_TABLE_CREATE('burst')         , V)
            ).end();

        })
        .cb(cb);

    }),

});

// }}}

// Book Operation {{{

fn.book = $P(function (arg, cb) {

    exec(SQL.BOOK_GET, { plan: plan })
    .map(function (book) {

        var def = deferred();

        exec(SQL.BOOK_TAG_GET, {
            plan: arg.plan,
            idbook: book.idbook
        })
        .end(function (result) {
            book.tags = result;
            def.resolve(book);
        });

        return def.promise;

    })
    .cb(cb);

});

_(fn.book).extend({

    add: $P(function (arg, cb) {

        exec(SQL.BOOK_ADD, {
            plan: arg.plan,
            idbook: arg.id,
            id: arg.id,
            name: arg.name,
            press: arg.press
        })
        (function (result) {
            var def = deferred();
            for(var tag in arg.tags) {
                var arg_tag = {
                    plan: arg.plan,
                    name: tag
                };
                def.then(
                    exec(TAG_ADD, arg_tag)
                    (function (result) {
                        if(result.insertId) {
                            return ;
                        }
                    })
                );
            }
        })
        .cb(cb);

    }),

    remove: $P(function () {

    }),

    update: $P(function () {

    }),

});

// }}}

// School Operation {{{

fn.school = $P(function (arg, cb) {

    exec(SQL.SCHOOL_GET, { plan: arg.plan })
    .cb(cb);

});

_(fn.school).extend({

    add: $P(function (arg, cb) {

        exec(SQL.SCHOOL_ADD, {
            plan: arg.plan,
            name: arg.name
        })
        .cb(cb);

    }),

    remove: $P(function () {

    }),

    update: $P(function () {

    }),

});

// }}}

// Order Operation {{{

fn.order = $P(function () {

});

_(fn.order).extend({

    add: $P(function () {

    }),

    remove: $P(function () {

    }),

    update: $P(function () {

    }),

});

// }}}

// Repository Operation {{{

fn.repo = $P(function () {
    
});

_(fn.repo).extend({

    add: $P(function () {

    }),

    remove: $P(function () {

    }),

    update: $P(function () {

    }),

});

// }}}

module.exports = fn;
