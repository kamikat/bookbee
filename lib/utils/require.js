
// Extend the require function

require.uncache = function (name) {
    delete require.cache[require.resolve(name)];
};

require.invalidate = function (name) {
    require.uncache(name);
    return require(name);
};

module.exports = require;
