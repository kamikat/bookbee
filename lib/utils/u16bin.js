
// Convert a runtime string to UTF-16 LE to be friendly for CJK
module.exports = function (str) {

    // Not All PDFReader support a Big Endian metadata string
    var swapBytes = function (buffer) {
        var l = buffer.length;
        if (l & 0x01) {
            throw new Error('Buffer length must be even');
        }
        for (var i = 0; i < l; i += 2) {
            var a = buffer[i];
            buffer[i] = buffer[i+1];
            buffer[i+1] = a;
        }
        return buffer;
    }
    return swapBytes(new Buffer("\ufeff" + str, 'ucs-2')).toString('binary');

}
