
var path = require('path');
var Font = require('../conf.d/font');

// Resolve Default Font Family
Font.Default = Font[Font.Default];

// Font Registration
Font.Register = function (doc) {
    for(var fontname in Font.Registry) {
        if(typeof Font.Registry[fontname] == 'string') {
            doc.registerFont(fontname, 
                             path.resolve(__dirname + '/../fonts/' + Font.Registry[fontname]));
        }else{
            doc.registerFont(fontname, 
                             path.resolve(__dirname + '/../fonts/' + Font.Registry[fontname][0]), 
                             Font.Registry[fontname][1]);
        }
    }
};

module.exports = Font;
