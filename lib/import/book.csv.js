
// Importer for Booklist from csv Exported from Excel
// | id | name | press | TAG1 | TAG2 ...

var csv = require('csv');
var book = require('./book');

var importer = function (db, data, callback) {
    var books = [];
    csv()
    .from(data, { delimiter: "\t" })
    .on('data', function(i, index){
        console.log('#'+index+' '+JSON.stringify(i));
        books.push({
            'id' : i[0], 
            'name' : i[1], 
            'press' : i[2], 
            'tags' : _.compact(i.slice(3)),
        });
    })
    .on('end',function(count){
        book(db, books, callback);
    })
    .on('error',function(err){
        callback(err);
    });
};

importer.name = "书籍信息导入";
importer.description = 
    "导入CSV文件的书籍信息。书籍信息包含统一征订号、名称、出版单位以及书籍标签";
importer.enabled = true;

module.exports = importer;
