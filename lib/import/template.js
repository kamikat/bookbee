
// Import

var importer = function (db, data, callback) {
    // Import operation
    // db - database object.
    // data - data with specific format
};

// Name of the importer
importer.name = "name";

// A short description to the importer
importer.description = 
    "description";

// Whether the importer is provided to user
importer.enabled = false;

module.exports = importer;
