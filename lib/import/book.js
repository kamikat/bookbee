
// Importer for Booklist with JSON Schema
// [{
//  id: "12345678",
//  name: "Textbook Name",
//  press: "Name of the Book's Press"
//  tags: [ "TAG1", ...]
// }, ...]

var _ = require('underscore');

var importer = function (db, data, callback) {

    var tags = [];
    data.forEach(function (i, index) {
        tags.push(i.tags);
    });
    tags = _(tags).chain()
    .uniq(false, function (a, b, c) {
        return a.join('');
    }).compact().value();

    db.Types.Book.Create(data, function (err) {
        
    });

    db.Types.Tags.Create(tags, function (err) {

    });

};

importer.name = "书籍信息导入";
importer.description = 
    "导入JSON表示的书籍信息。书籍信息包含统一征订号、名称、出版单位以及书籍标签";
importer.enabled = false;

module.exports = importer;
