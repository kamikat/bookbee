
// Usage: 
//  require('<this module>');
//  var PDFDocument = require('pdfkit');
//  var doc = new PDFDocument();
//  ...

var PDFDocument = require('pdfkit');

// Work-for:
//  The width of CJK character is ill calculated, resulting in centering layout error
// widthOfString workaround for Monospace CJK Fonts (typically) {{{

// Preserve reference to original function
var _widthOfString = PDFDocument.prototype.widthOfString;

// function to convert all DBC to \u3000
var ToU3000 = function (str) {  

    // Recognize CJK no half-width Katakana
    var isDbcCase = function (c) {  
        if (c >= 32 && c <= 127) {  
            return true;  
        } else if (c >= 65377 && c <= 65439) {  
            return true;  
        }  
        return false;  
    }
    var len = [];  
    var c;
    for (var i=0;i<str.length;i++){  
        c = str.charCodeAt(i);  
        if (!isDbcCase(c)) {
            len.push('\u3000');
        }else{
            len.push(str[i]);
        }  
    }
    return len.join('');  
}

PDFDocument.prototype.widthOfString = function (str) {
    arguments[0] = ToU3000(str);
    return _widthOfString.apply(this, arguments);
}

// }}}

var Subset = require('pdfkit/js/font/subset');

// Work-for: 
//  Fontface Miss when embedded character in a subset grows over 126
// Solution Issue:
//  Work only for Foxit Reader & Google Chrome
// PDFKit Sharding of PDFFont Subset {{{

var _use = Subset.prototype.use;

Subset.prototype.use = function (character) {

    if (this.next == 127) {
        this.next++;
    }

    return _use.apply(this, arguments);

};

// }}}

