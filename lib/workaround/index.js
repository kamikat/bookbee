
require('require-all')({
    dirname     :  __dirname,
    filter      :  /((?:(?!index.js$)).+)\.js$/,
    excludeDirs :  /^\.(git|svn)$/
});
