var exp = require('express');
var crypto = require('crypto');
var con = require('console');
var path = require('path');
var fs = require('fs');
// var db = require('./json-db')

var sessionSecretSeed = crypto.randomBytes(16).toString('hex');

var app = exp();

app.configure(function(){
    app.set('views', path.resolve(__dirname + '/../templates'))
    app.use(exp.bodyParser());
    app.use(exp.cookieParser());
    app.use(exp.compress());
    app.use(exp.session({ secret : sessionSecretSeed }));
    app.use(app.router);
    app.use(exp.static(path.resolve(__dirname + '/../static')));
});

app.get("/", function(req, res){
    res.render("content.jade");
});

app.get("/json/:id/:version.js", function(req, res){
    res.charset="utf-8";
    fs.readFile(path.resolve(__dirname + '/../stub/data/' + req.params.id + '.json'),
                function (err, data) {
                    if (err) throw err;
                    res.json(200, JSON.parse(data));
                });
                con.info("Requested: " + req.params.id + " at Version " + req.params.version);
});

app.get("/report/:type", function(req, res) {

});

// Initialization completed

module.exports = app;
