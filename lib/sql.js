
// SQL Commands
// ============

var SQL = {

    PLAN_ADD    : 'INSERT INTO plan SET name = :name',
    PLAN_GET    : 'SELECT * FROM plan',

    PLAN_TABLE_CREATE: function (table) {
        return 'CREATE TABLE IF NOT EXISTS ' + 
        'bookbee.{PLAN}$TABLE LIKE bookbee.$TABLE'
        .replace(/\$TABLE/gi, table);
    },

    BOOK_GET    : 'SELECT * FROM {PLAN}book',
    BOOK_TAG_GET: 
        'SELECT {PLAN}tag.* \
         FROM   {PLAN}book_has_tag, {PLAN}tag \
         WHERE  {PLAN}book_has_tag.tag_idtag = {PLAN}tag.idtag \
           AND  {PLAN}book_has_tag.book_idbook = :idbook',
    BOOK_ADD    : 
        'INSERT INTO {PLAN}book \
         VALUES (:idbook, :id, :name, :press)',
    TAG_ADD     :
        'INSERT IGNORE INTO {PLAN}tag SET name = :name',
    TAG_GET     : 'SELECT * FROM {PLAN}tag WHERE name = :name',

    SCHOOL_GET  : 'SELECT * FROM {PLAN}school',
    SCHOOL_ADD  : 'INSERT INTO {PLAN}school SET name = :name',

};

// SQL Command Format Definition {{{

SQL.CommandResolver = function (query, values) {

    if (!values) return query;

    // Escape Plan Id Variable
    if(values.plan) {
        query = query.replace(/{PLAN}/gi, '_' + values.plan);
    }

    return query.replace(/\:(\w+)/g, function (txt, key) {
        if (values.hasOwnProperty(key)) {
            return this.escape(values[key]);
        }
        return txt;
    }.bind(this));

};

// }}}

module.exports = SQL;
