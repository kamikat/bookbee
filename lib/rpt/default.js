#!/usr/bin/env nodejs

var Font = require('../font');

// Variable Definition Section //{{{
var should = require('should');

// Load Workaround Module
require('../workaround/pdfkit');

var pdf = require('pdfkit');
var _ = require('underscore');
var path = require('path');
var u16bin = require('../utils/u16bin');

var date = new Date();
//}}}

// Paper Definition: A4, Margin: 64
var DOCUMENT_PAGE_DEF = {size: 'LETTER', margins: {left: 64, top: 64, right: 64, bottom: 64}};

// Lines not including HEADER per page
var LINES_PER_PAGE = 30;

// HEADER - Header on each page //{{{
var HEADER = function (doc, ctx) {

    // Draw the Title and Initialize context Variables
    var contentWidth = ctx.contentWidth = doc.page.width - doc.page.margins.left - doc.page.margins.right;

    var school = ctx.school;
    var tag = _(ctx.taglist).chain().flatten().uniq().value().join('-');
    var sources = ctx.srclist.join('/');

    doc
    .font(Font.Default.Bold, 16)
    .text("福鼎市新华书店发货清单", { width: contentWidth, align: 'center' })
    .moveDown()
    .font(Font.Default.Regular, 10)
    .text("收货单位：" + school + " (" + tag + ")")
    .moveUp()
    .text(date.getFullYear() + ' 年 ' + 
          (date.getMonth() + 1) + ' 月 ' +
          date.getDate() + ' 日 ', { width: contentWidth, align: 'right' });

    Stroke(doc, 2);

    ctx._LineCounter = 0;

    // Header Draw Requested by Summary
    if(ctx.summary !== undefined) return;

    Line(doc, ctx, ["征订号", "书目", "定价", "规格", "册数", "码洋", "包捆零"]);

};
//}}}

// FOOTER - Footer on each page //{{{
var FOOTER = function (doc, ctx, d) {
    doc.x = doc.page.margins.left;
    doc.y = doc.page.maxY();
    doc
    .font(Font.Default.Regular, 10)
    .moveUp();
    doc.text('第 ' + doc.pages.length + ' 页', {align: 'center'});
};
//}}}

// SUMMARY - Summary Section on Last Page //{{{
var SUMMARY = function (doc, ctx, d) {
    doc.y -= 6; Stroke(doc, 2);
    var spacelines = LINES_PER_PAGE - ctx._LineCounter;
    if(spacelines < 2) {
        // Not Enough Space to put summary, New Page.
        ctx.summary = d;

        FOOTER(doc, ctx, {
            date: new Date(),
            page: doc.pages.length
        });
        doc.addPage();
        HEADER(doc, ctx);
        spacelines = LINES_PER_PAGE - ctx._LineCounter;
    }
    doc
    .moveDown()
    .font(Font.Default.Regular, 12);
    doc.x += 20;
    doc.text("品种：" + d.variety).moveUp();
    doc.x += 80;
    doc.text("发货数：" + d.amount).moveUp();
    doc.x += 110;
    doc.text("码洋：" + d.consult.toFixed(2)).moveUp();
    doc.x += 120;
    doc.text(d.packages + "\t件 " + d.bundles + "\t捆 " + d.pieces + "\t本");
    doc.x = doc.page.margins.left;
    if(spacelines >= 6) {
        doc.moveDown().moveDown()
        .font(Font.Default.Bold, 12)
        .text("收货人签章（签名）", doc.page.width - doc.page.margins.right - 250);
    }
};
//}}}

// Stroke - Draw a Hirizonal line cross the page //{{{
var Stroke = function (doc, thickness) {
    if(thickness === undefined) thickness = 1;
    doc.y += 3;
    doc
    .save()
    .lineCap('round').lineWidth(thickness)
    .moveTo(doc.page.margins.left - 5, doc.y)
    .lineTo(doc.page.width - doc.page.margins.right + 5, doc.y)
    .stroke();
    doc.y += 3;
    doc.x = doc.page.margins.left;
};
//}}}

var colWidths = [80, 172, 40, 45, 40, 55, 55];

// Line - Write a Line of data into Doc //{{{
var Line = function (doc, ctx, d) {

    var contentWidth = ctx.contentWidth;

    for(var i = 0; i != colWidths.length; i++) {
        doc
        .text(d[i], { width: 400 })
        .moveUp();
        if(doc.widthOfString(d[i]) > colWidths[i]) {
            // Measure if a linebreak is needed
            doc.moveDown(); doc.y += 3; ctx._LineCounter++;
        }
        doc.x += colWidths[i];
    }

    doc.moveDown(); ctx._LineCounter++;

    if(ctx._LineCounter < LINES_PER_PAGE) {
        Stroke(doc);
    }else{
        Stroke(doc, 2);
        FOOTER(doc, ctx, {
            date: new Date(),
            page: doc.pages.length
        });
        doc.addPage();
        HEADER(doc, ctx);
    }

};
//}}}

var produce = function (d, school, taglist, srclist) {

    taglist = _(taglist).map(function (a) { return a.join('>'); });

    var doc = new pdf(DOCUMENT_PAGE_DEF);

    Font.Register(doc);

    var ctx = {};

    // Apply HEADER
    HEADER(doc, ctx = {school: school, taglist: taglist, srclist: srclist});

    var summary = {
        variety: 0,
        amount: 0,
        consult: 0,
        packages: 0, bundles: 0, pieces: 0 
    };

    var book = _.sortBy(d.book, function(data) { return data.id; } );

    // Generate Body
    _.each(book, function (item) {
        if(item.id === '') return;
        if(!_(taglist).include(item.tags.join('>'))) return;
        var repo = d.repo[item.id];
        var order = d.order[item.id];
        if(repo === undefined) return;
        if(order === undefined) return;
        if(order[school] === undefined) return;
        if(!_(srclist).include(repo.source)) return;
        var id = item.id;
        var name = item.name;
        var price = repo.price;
        var spec = repo.wrap.package + 'x' + repo.wrap.bundle;
        var amount = order[school];
        var consult = (amount * price);
        var a = Math.floor(amount / repo.wrap.bundle / repo.wrap.package),
        b = Math.floor(amount / repo.wrap.bundle) % repo.wrap.package,
        c = amount % repo.wrap.bundle,
        pkg = a + '+' + b + '+' + c;
        summary.variety++;
        summary.amount += amount; 
        summary.consult += consult; 
        summary.packages += a; summary.bundles += b; summary.pieces += c;
        Line(doc, ctx, [id, name, price, spec, amount, consult.toFixed(2), pkg]);
    }); 

    SUMMARY(doc, ctx, summary);

    FOOTER(doc, ctx, {
        date: new Date(),
        page: doc.pages.length
    });

    ctx.summary = summary;

    var title = "福鼎市新华书店发货清单 - " + school;
    var subject = "新华书店发货清单 " + school + " (第 " + srclist.join('/') + " 批) " + summary.variety + "种";
    var author = "BookBee PM System";
    var keywords = _(taglist).chain().flatten().uniq().value().join(';').replace(/\>/g, ',');
    var creator = "BookBee Default Report Template";
    var producer = "BookBee Generator - PDFKit";
    doc.info.Title = u16bin(title);
    doc.info.Subject = u16bin(subject);
    doc.info.Author = u16bin(author);
    doc.info.Keywords = u16bin(keywords);
    doc.info.Creator = u16bin(creator);
    doc.info.Producer = u16bin(producer);


    return {config: ctx, doc: doc};
};

module.exports = function (req, callback) {
    if(callback === undefined) callback = function () {};
    // Parameter Check
    should.exist(req);
    req.should.have.property("book");
    req.should.have.property("school");
    req.should.have.property("order");
    req.should.have.property("repo");
    req.should.have.property("tag");
    req.should.have.property("reposrc");
    req.should.have.property("query");

    var result = produce(req, req.query.school, req.query.tag, req.query.reposrc);

    callback(result);
};
