// Test Program run with Mocha

var should = require('should');
var _ = require('underscore');

describe("Default Report", function () {

    var target = require('../lib/rpt/default');
    // target is a function to generate a default report

    var fetch = require('../stub/data');

    it('should throw error when argument not suitable', function () {//{{{

        // Call with illegal argument
        (function () {
            target();
            target({});
            target({
                book: [],
                school: [],
                order: {},
                repository: {}
            });
        }).should.throwError(/^expected/);

    });//}}}

    // Generate PDF - Generate a pdf file with all sources //{{{
    var GeneratePDF = function (data, school, tag, reposrc, ok, start) {
        if(start === undefined) start = function () {};
        if(ok === undefined) ok = function () {};
        target({
            book: data.book,
            school: data.school,
            order: data.order,
            repo: data.repo,
            reposrc: data.reposrc,
            tag: data.tag,
            query: {
                school: school,
                reposrc: reposrc,  // All sources
                tag: tag
            }
        }, function (result) {
            should.exist(result);
            should.exist(result.doc);
            should.exist(result.config);
            if(result.config.summary.variety == 0) return;
            start();
            result.doc.write(__dirname + "/gen/" + 
                             result.config.school + "(" + 
                             result.config.taglist.join(';').replace(/\>/g,',') + ").pdf"
            , ok);
            console.log('\tGenerated ' + result.config.school + "(" +
                        result.config.taglist.join(';').replace(/\>/g,',') + ").pdf");
        });
    };//}}}

    it('should generate one pdf file', function (done) {//{{{
        fetch(function (data) {
            var c = 2;
            var ok = function () { c--; if(c==0) done(); };
            GeneratePDF(data, "桐北", data.tag, data.reposrc, ok);
            GeneratePDF(data, "桐北", [data.tag[0]], data.reposrc, ok);
        });
    });//}}}

    it.skip('should generate pdf files for each school of each tag group with all sources', function (done) {//{{{
        fetch(function (data) {
            var tc = 0;
            var start = function () { tc++; };
            var ok = function () {
                tc--;
                if(tc == 0) { done(); }
            };
            console.log('');
            // data loaded
            _(data.school).each(function (school) {
                _(data.tag).each(function (tag) {
                    GeneratePDF(data, school, [tag], data.reposrc, ok, start);
                });
            });
        });
    });//}}}

    it.skip('should generate pdf files for each school of each tag group with specific sources', function (done) {//{{{
        fetch(function (data) {
            var tc = 0;
            var start = function () { tc++; };
            var ok = function () {
                tc--;
                if(tc == 0) { done(); }
            };
            // data loaded
            _(data.school).each(function (school) {
                _(data.tag).each(function (tag) {
                    GeneratePDF(data, school, [tag], ['2012-09-07','2012-09-07-2'], ok, start);
                });
            });
        });
    });//}}}

    it('should return {"status": <code>, "path": <path/to/file>, "progress": <0-100>} when in progress or finished');

    it('should merge multiple same requests into same working package when recieve multiple requests');

    it('should cache the generated pdf file and return status code immendiately when requested and cache exists');

});
