
var should = require('should');

describe("pdfkit", function () {

    require('../lib/workaround/pdfkit');
    var pdf = require('pdfkit');
    var path = require('path');

    describe("PDFDocument", function () {

        var doc = new pdf();
        doc.info['Title'] = "Test Document for PDFKit";
        doc.info['Author'] = 'BookBee';
        doc.registerFont('Normal', path.resolve(__dirname + '/../fonts/msyh.ttf'));
        doc.registerFont('Bold', path.resolve(__dirname + '/../fonts/msyhbd.ttf'));

        describe("#widthOfString", function () {

            it('should return different value when test with CJK char and ASCII char', function () {//{{{
                doc.font('Normal',10).widthOfString('A')
                .should.not.equal(
                    doc.font('Normal',10).widthOfString('啊')
                );
            });//}}}

            it('should return same value when test with different CJK char', function () {//{{{
                doc.font('Normal',10).widthOfString('哦')
                .should.equal(
                    doc.font('Normal',10).widthOfString('お')
                );
                doc.font('Normal',10).widthOfString('福')
                .should.not.equal(0);
            });//}}}

        });
    });
});
