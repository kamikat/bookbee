#!/usr/bin/env node

var csv = require('csv');
var _ = require('underscore');
var path = require('path');
var fs = require('fs');

var files = process.argv.slice(2);

console.log('Files: '+JSON.stringify(files));

var repo = {};

fs.readFile(path.resolve(__dirname + '/../repository.json'),
           function (err, data) {
               if (err) {
               }else{
                   try{
                       repo = JSON.parse(data);
                   }finally{}
               }
               processInput();
          }); 

processInput = function () {
files.forEach(function (file) {

    console.log(file + " => " + path.resolve(file));
    var filename = path.basename(file, ".csv");
    var visited = _([]);

    csv()
    .from.path(path.resolve(file))
    .on('record',function(data,index){
        if(!visited.contains(data[0])) {
            visited.push(data[0]);
            repo[data[0]] = {};
        }
        if('object' != typeof repo[data[0]]) {
            console.log('TYPE of Target ' + typeof repo[data[0]]);
            repo[data[0]] = {};
        }
        repo[data[0]].price = data[2];
        repo[data[0]].wrap = {};
        repo[data[0]].wrap.package = data[3];
        repo[data[0]].wrap.bundle = data[4];
        var n = parseInt(data[5]);
        if(_.isNaN(n)) n = 0;
        if('number' != typeof repo[data[0]].amount)
            repo[data[0]].amount = 0;
        repo[data[0]].amount += n;
        repo[data[0]].source = filename;
        console.log('#'+index+' '+JSON.stringify(repo[data[0]]));
    })
    .on('end',function(count){
        fs.writeFile(path.resolve(__dirname + '/../repository.json'), JSON.stringify(repo));
    })
    .on('error',function(error){
        console.log(error.message);
    });

});
}
