#!/usr/bin/env node

var csv = require('csv');
var _ = require('underscore');

var books = [];
var tags = [];

csv()
.from.path(__dirname+'/book.csv', { delimiter: "\t" })
.on('record',function(data,index){
    console.log('#'+index+' '+JSON.stringify(data));
    books.push({
        'id' : data[0], 
        'name' : data[1], 
        'press' : data[2], 
        'tags' : _.compact(data.slice(3)),
    });
    tags.push(books[books.length - 1].tags);
})
.on('end',function(count){
    console.log('Number of books: '+count);
    var fs = require('fs');
    fs.writeFile(__dirname+'/book.json', JSON.stringify(books));
    fs.writeFile(__dirname+'/tag.json', JSON.stringify(_(tags).chain()
    .uniq(false, function (a, b, c) { 
        return a.join(''); 
    }).compact().value()));
})
.on('error',function(error){
    console.log(error.message);
});
