#!/usr/bin/env node

var csv = require('csv');

var schools = [];

csv()
.from.path(__dirname+'/school.csv')
.on('record',function(data,index){
    console.log('#'+index+' '+JSON.stringify(data));
    schools.push(data[0]);
})
.on('end',function(count){
    console.log('Number of schools: '+count);
    var fs = require('fs');
    fs.writeFile(__dirname+'/school.json', JSON.stringify(schools));
})
.on('error',function(error){
    console.log(error.message);
});
