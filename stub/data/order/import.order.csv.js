#!/usr/bin/env node

var csv = require('csv');
var _ = require('underscore');
var path = require('path');
var fs = require('fs');

var files = process.argv.slice(2);

console.log('Files: '+JSON.stringify(files));

var order = {};

fs.readFile(path.resolve(__dirname + '/../order.json'),
           function (err, data) {
               if (err) {
               }else{
                   order = JSON.parse(data);
               }
               processInput();
          }); 

processInput = function () {
files.forEach(function (file) {

    var schools = undefined;
    console.log(file + " => " + path.resolve(file));

    csv()
    .from.path(path.resolve(file), {
        delimiter: "\t"
    })
    .on('record',function(data,index){
        console.log('#'+index+' '+JSON.stringify(data));
        if(schools == undefined) {
            schools = data.slice(1);
            return;
        }
        if('object' != typeof order[data[0]]) {
            console.log('TYPE of Target ' + typeof order[data[0]]);
            order[data[0]] = {};
        }
        values = data.slice(1);
        for(i = 0; i != schools.length; i++) {
            var val = parseInt(values[i]);
            if(val != 0 && !_.isNaN(val) && 'number' == typeof(val)) {
                order[data[0]][schools[i]] = parseInt(values[i]);
            }
        }
    })
    .on('end',function(count){
        fs.writeFile(path.resolve(__dirname + '/../order.json'), JSON.stringify(order));
    })
    .on('error',function(error){
        console.log(error.message);
    });

});
}
