// Test module to load all data under data directory
// callback `done' will be called with an object
module.exports = (function (done) {

    var fs = require('fs');
    var _ = require('underscore');
    var path = require('path');

    var sync = [
        function (d) {
        fs.readFile(path.resolve(__dirname + '/data/school.json'),
                    function (err, data) {
                        if (err) {
                            throw err;
                        }else{
                            d.school = JSON.parse(data);
                        }   
                        sync.shift()(d);
                    }); 
    },
    function (d) {
        fs.readFile(path.resolve(__dirname + '/data/book.json'),
                    function (err, data) {
                        if (err) {
                            throw err;
                        }else{
                            d.book = JSON.parse(data);
                        }   
                        sync.shift()(d);
                    }); 
    },
    function (d) {
        fs.readFile(path.resolve(__dirname + '/data/tag.json'),
                    function (err, data) {
                        if (err) {
                            throw err;
                        }else{
                            d.tag = JSON.parse(data);
                        }   
                        sync.shift()(d);
                    }); 
    },
    function (d) {
        fs.readFile(path.resolve(__dirname + '/data/order.json'),
                    function (err, data) {
                        if (err) {
                            throw err;
                        }else{
                            d.order = JSON.parse(data);
                        }   
                        sync.shift()(d);
                    }); 
    },
    function (d) {
        fs.readFile(path.resolve(__dirname + '/data/repository.json'),
                    function (err, data) {
                        if (err) {
                            throw err;
                        }else{
                            d.repo = JSON.parse(data);
                            d.reposrc = _(d.repo).chain().values().pluck('source').uniq().value();
                        }   
                        done(d);
                    }); 
    },
    ];

    sync.shift()({});

});
